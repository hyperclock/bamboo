<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
  /**
   * @Route("/{id}", name="blog_show", methods={"GET"}, requirements={"id"="\d+"})
   */
  public function show($id)
  {
      $em = $this->getDoctrine()->getManager();

      $blog = $em->getRepository('App:Blog')->find($id);

      if (!$blog) {
      throw $this->createNotFoundException('Unable to find Blog post.');
      }

      $comments = $em->getRepository('App:Comment')
                     ->getCommentsForBlog($blog->getId());
//                     ->findByComment($blog->getId());

      return $this->render('blog/show.html.twig', array(
          'blog'      => $blog,
          'comments'  => $comments
      ));

  }

}
